import avatar from "./assets/images/avatar.jpeg"
import cssModule from './App.module.css'

function App() {
  return (
    <div className={cssModule.devcamp}>
        <div className={cssModule.devcampWrapper}>
          <img className={cssModule.devcampAvatar} src={avatar} alt="Tammy Stevens"></img>
          <div className={cssModule.devcampQuote}>
            <p>
              This is one of the best developer blogs on the planet! I read it daily to improve my skills
            </p>
          </div>
          <p className={cssModule.devcampName}>
            Tammy Stevens<span className={cssModule.devcampInfo}> - Front End Developer</span>
          </p>
        </div>
      </div>
      );
}

      export default App;
